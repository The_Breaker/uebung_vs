<?php
/**
 * Created by PhpStorm.
 * User: vladi
 * Date: 16.12.16
 * Time: 09:16
 */

$bin = trim(fgets(STDIN));

function binToDec($bin) {
    $array = array_reverse(array_map('intval',str_split($bin)));
    $bin_value = [];
    foreach($array as $key => $value){

        if($value != 0){

            array_push($bin_value, pow(2,$key));
        }
    }
    $dec=array_sum($bin_value);

    print_r($dec);
}

binToDec($bin);