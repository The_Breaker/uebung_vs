<?php
/**
 * Created by PhpStorm.
 * User: vladi
 * Date: 20.12.16
 * Time: 13:17
 */
$n = 4;
$ende = 10;
function compute ($n)
{
    if($n<=0)
        return 0;
    else if($n==1)
        return 1;
    else
    {
        $a=0;
        $b=1;
        $i=2;
        while($i<=$n)
        {
            $aa=$b;
            $bb=$a+$b;
            $a=$aa;
            $b=$bb;
            $i++;
        }
        return $b;
    }
}
while($n <= $ende){
    print_r("F(".$n.") = ".compute($n).PHP_EOL);
    $n++;
}
