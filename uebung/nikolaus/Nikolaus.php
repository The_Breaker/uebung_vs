<?php

/**
 * Created by PhpStorm.
 * User: vladi
 * Date: 21.12.16
 * Time: 15:34
 */
class Nikolaus
{

    protected $_bag = [];

    public function __construct()
    {
        $a = func_get_args();
        $i = func_num_args();
        if (method_exists($this,$f='__construct'.$i)) {
            call_user_func_array(array($this,$f),$a);
        }else{
            $nut = str_repeat("nut",20);
            $this->setBag(str_split($nut, 3));
            print_r($this->getBag());
        }
    }

    public function __construct1($nuts)
    {
        $nut = str_repeat("nut", $nuts);
        $this->setBag(str_split($nut, 3));
        print_r($this->getBag());
    }

    public function ClearBag($clear)
    {
        $empty = [];
        $star = [];
        for($x = 1; $x <= $clear; $x++)
        {
            array_push($star, '*');
        }
        $bag = $this->getBag();
        if(array_search("*", $bag) != false)
        {

            foreach($bag as $string)
            {
                if($string == "*")
                {
                    array_push($empty, $string);
                }
            }
            $bag = array_diff($bag, ["*"]);

        }
        $this->setBag(array_splice($bag, -$clear, $clear, $star));

        $this->setBag(array_merge($bag, $empty));
        print_r($this->getBag());

    }

    public function FillBag($fill)
    {
        $bag = $this->getBag();
        $count = 0;
        $end = 0;
        //überprüft ob es leeren Platz im Beutel gibt
        if(array_search("*", $bag) != False)
        {
            foreach($bag as $value){
                if($value == "*" && $fill != $end )
                {
                    unset($bag[$count]);
                    $end++;
                }
                $count++;
            }
        }
        $nut = str_split(str_repeat("nut",$fill), 3);
        $this->setBag(array_merge($bag,$nut));
        print_r($this->getBag());
    }

    public function content()
    {
        $bag = $this->getBag();

        $bag = array_diff($bag, ["*"]);

        return print_r("There are ".count($bag)." nuts in the bag".PHP_EOL);

    }

    public function look()
    {
        $count = 0;
        $bag = $this->getBag();

        $bag = array_diff($bag, ["*"]);

        foreach($bag as $value)
        {
            $bag[$count] = "*";

            $count++;
        }

        print_r($bag);
    }

    public function getBag()
    {
        return $this->_bag;
    }

    public function setBag($bag)
    {
        $this->_bag = $bag;
    }

}