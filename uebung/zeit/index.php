<?php
/**
 * Created by PhpStorm.
 * User: vladi
 * Date: 21.12.16
 * Time: 09:43
 */
include "Zeit.php";

$zeit = new Zeit();
$ende = 0;

do{
    print_r("Was wuenschen Sie: ".PHP_EOL);
    print_r("1 = Zeiteingabe ".PHP_EOL);
    print_r("2 = Zeitanzeige ".PHP_EOL);
    print_r("3 = Erhöhe Sekunden ".PHP_EOL);
    print_r("4 = Erhöhe Minuten ".PHP_EOL);
    print_r("5 = Erhöhe Stunden ".PHP_EOL);
    print_r("6 = Addiere weitere Zeit ".PHP_EOL);
    print_r("7 = Setze zurück ".PHP_EOL);
    print_r("8 = Systemzeit ".PHP_EOL);
    print_r("9 = Ende ".PHP_EOL);
    $zahl = trim(fgets(STDIN));


    switch($zahl)
    {
        case 1: $zeit->Eingabe();
            break;
        case 2: $zeit->Ausgabe();
            break;
        case 3: $zeit->IncrementSekunden();
            break;
        case 4: $zeit->IncrementMinuten();
            break;
        case 5: $zeit->IncrementStunden();
            break;
        case 6: $zeit->Add();
            break;
        case 7: $zeit->Zurücksetzen();
            break;
        case 8: $zeit->Systemzeit();
            break;
        case 9: $ende = 9;
            break;
        default: print_r("falsche Eingabe. Versuchen Sie es erneut.".PHP_EOL);
            break;

    }

}while($ende == 0);
