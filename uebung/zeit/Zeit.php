<?php
/**
 * Created by PhpStorm.
 * User: vladi
 * Date: 21.12.16
 * Time: 09:15
 */

class Zeit
{
    protected $_stunden = 0;

    protected $_minuten = 0;

    protected $_sekunden = 0;

    public function Eingabe()
    {
        echo "Bitte geben Sie eine Zeit ein ".PHP_EOL;
        echo "Sekunden: ".PHP_EOL;
        fscanf(STDIN, "%d\n", $iSekunden);
        echo "Minuten: ".PHP_EOL;
        fscanf(STDIN, "%d\n", $iMinuten);
        echo "Stunden: ".PHP_EOL;
        fscanf(STDIN, "%d\n", $iStunden);

        $this->setSekunden($iSekunden);
        $this->setMinuten($iMinuten);
        $this->setStunden($iStunden);
    }

    public function Ausgabe()
    {
        $date = new DateTime();

        $date->setTime($this->getStunden(),$this->getMinuten(),$this->getSekunden());

        echo $date->format("H:i:s").PHP_EOL;
    }

    public function IncrementSekunden()
    {
        echo "Wie viele Sekunden: ";
        fscanf(STDIN, "%d\n", $incrSec);

        $this->setSekunden($this->getSekunden() + $incrSec);

    }

    public function IncrementMinuten()
    {
        echo "Wie viele Minuten: ";
        fscanf(STDIN, "%d\n", $incrMin);

        $this->setMinuten($this->getMinuten() + $incrMin);

    }

    public function IncrementStunden()
    {
        echo "Wie viele Stunden: ";
        fscanf(STDIN, "%d\n", $incrH);

        $this->setStunden($this->getStunden() + $incrH);

    }

    public function Add()
    {
        echo "Wie viel soll dazu addiert werden: ".PHP_EOL;
        echo "Sekunden: ".PHP_EOL;
        fscanf(STDIN, "%d\n", $addSek);
        echo "Minuten: ".PHP_EOL;
        fscanf(STDIN, "%d\n", $addMin);
        echo "Stunden: ".PHP_EOL;
        fscanf(STDIN, "%d\n", $addH);

        $this->setSekunden($this->getSekunden() + $addSek);
        $this->setMinuten($this->getMinuten() + $addMin);
        $this->setStunden($this->getStunden() + $addH);

    }

    public function Zurücksetzen()
    {
        $this->setStunden(0);
        $this->setMinuten(0);
        $this->setSekunden(0);
    }

    public function Systemzeit()
    {
        date_default_timezone_set('Europe/Berlin');
        date($this->setStunden("H").$this->setMinuten("i").$this->setSekunden("s"));

    }


    public function getStunden()
    {
        return $this->_stunden;
    }

    public function setStunden($stunden)
    {
        $this->_stunden = $stunden;
    }

    public function getMinuten()
    {
        return $this->_minuten;
    }

    public function setMinuten($minuten)
    {
        $this->_minuten = $minuten;
    }

    public function getSekunden()
    {
        return $this->_sekunden;
    }

    public function setSekunden($sekunden)
    {
        $this->_sekunden = $sekunden;
    }
}