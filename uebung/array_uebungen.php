<?php
/**
 * Created by PhpStorm.
 * User: vladi
 * Date: 25.11.16
 * Time: 12:38
 */

$iarray = [];
$reverseArray = [];

// Aufgabe a
for($x = 0; $x < 10; $x++)
{
    $randomNumber = mt_rand(1, 20);
    array_push($iarray, $randomNumber);

}

// Aufgabe b
$average = round(array_sum($iarray) / count($iarray));

// Aufgabe c
$reverse = array_reverse($iarray);

array_push($reverseArray, $reverse);
// Aufgabe d
function larger($var){
    $uniqueNumber = mt_rand(1, 20);
    return($var > $uniqueNumber);

}

//print_r(array_filter($iarray, "larger"));

// Aufgabe e
$max = max($iarray);
$min = min($iarray);

print_r("Max: " .$max. " Min: " .$min);

