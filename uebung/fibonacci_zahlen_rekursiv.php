<?php
/**
 * Created by PhpStorm.
 * User: vladi
 * Date: 19.12.16
 * Time: 17:24
 */

$anfang = 1;
$ende = 10;

function compute($anfang){
    if($anfang<=0){
        return 0;
    }elseif($anfang == 1) {
        return 1;
    }else {
        return compute($anfang-2) + compute($anfang-1);
    }

}
while($anfang <= $ende){
    print_r("F(".$anfang.") = ".compute($anfang).PHP_EOL);
    $anfang++;
}
