<?php
/**
 * Created by PhpStorm.
 * User: vladi
 * Date: 15.12.16
 * Time: 14:10
 */
$string = trim(fgets(STDIN));
function isbn($string){
    $array_every_third = [];
    $array_rest = [];

    $int = intval(preg_replace('/[^0-9]+/','',$string));
    $array = array_map('intval',str_split($int));

    foreach($array as $key => $value){

        if($key % 3 === 0){
            $multiply = $value * 3;
            array_push($array_every_third, $multiply);

        }else {
            array_push($array_rest, $value);
        }


    }
    $summe1 = array_sum($array_every_third);
    $summe2 = array_sum($array_rest);

    $ziffer =10 - (($summe1 + $summe2) % 10);
    print_r("Deine Pruefziffer ist: ".$ziffer.PHP_EOL);

}

isbn($string);