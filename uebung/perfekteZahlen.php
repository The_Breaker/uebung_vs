<?php
/**
 * Created by PhpStorm.
 * User: vladi
 * Date: 19.12.16
 * Time: 15:08
 */

for($x = 1; $x <= 100; $x++){
    $perfect = [];
    for($y = 1; $y < 100; $y++){
        if($x != $y){
            if($x % $y === 0){
                array_push($perfect, $y);
            }
        }
    }
    if(array_sum($perfect) === $x) {
        print_r("Perfekte Zahl: " . $x . PHP_EOL);
    }
}
