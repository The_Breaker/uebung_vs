<?php

$aSportartikel = array(
    'Fussballtrikots' => array(
        'Produkt_1' => array(
            'name' => 'Borussia Mönchengladbach Retro T-Shirt ',
            'preis' => ' € 29.95'
        ),
        'Produkt_2' => array(
            'name' => 'Eintracht Frankfurt Trikot Home Stadium',
            'preis' => ' € 84.95'
        ),
        'Produkt_3' => array(
            'name' => 'Hamburger SV Trikot Away ',
            'preis' => ' € 84.95'
        ),
        'Produkt_4' => array(
            'name' => 'SV Werder Bremen Trikot Home ',
            'preis' => ' € 84.95'
        ),
        'Produkt_5' => array(
            'name' => 'Borussia Dortmund Trikot Home',
            'preis' => ' € 27.95'
        ),
    ),
    'Fußballschuhe' => array(
        'Produkt 1' => array(
         'name' => 'X 16.1 FG Fußballschuh',
         'preis' => '€ 199.95'
        ),
        'Produkt 2' => array(
         'name' => 'ACE 16+ Purecontrol FG Fußballschuh',
         'preis' => '€ 299.95'
        ),
        'Produkt 3' => array(
         'name' => 'X 16+ Purechaos FG Fußballschuh',
         'preis' => '€ 199.95'
        ),
        'Produkt 4' => array(
         'name' => 'X 16.3 FG Fußballschuh',
         'preis' => '€ 59.95'
        ),
        'Produkt 5' => array(
         'name' => 'X 16.1 SG Fußballschuh',
         'preis' => '€ 199.95'
        ),
    ),
    'Fussballsocken' => array(
        'Produkt 1' => array(
            'name' => 'Match Fit Elite Hypervenom Crew Socken',
            'preis' => '€ 13.95'
        ),
        'Produkt 2' => array(
            'name' => 'Stadium Football OTC Sockenstutzen',
            'preis' => '€ 11.95'
        )
        ,'Produkt 3' => array(
            'name' => 'Classic II Sockenstutzen',
            'preis' => '€ 9.95'
        )
        ,'Produkt 4' => array(
            'name' => 'Match Fit Elite Hypervenom Crew Socken',
            'preis' => '€ 13.95'
        ),
    )
);

?>


<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>CSS und HTML Übungen</title>

        <link rel="stylesheet" href="style.css">
    </head>
    <body>
        <h1> Sportartikel</h1>
        <div id="rechts">
            <ul>

                <li> <a href="Startseite.html" target="_blank"> Startseite</a></li>
                <li> <a href="Kontakte.html" target="_blank"> Kontakte</a></li>
                <li> <a href="Impressum.html" target="_blank"> Impressum</a></li>
            </ul>

        </div>
        <table border="1">

            <thead>

                <tr>
                    <th> Sportsachen </th>
                    <th> Preis </th>
                </tr>

            </thead>
            <tbody>
                <?php
                    foreach($aSportartikel as $key => $value)
                    {
                ?>
                        <tr>
                            <td class="headline" colspan="2">
                                <b> <?php echo $key; ?> </b>
                            </td>
                        </tr>
                <?php
                        foreach($value as $produkt) {
                ?>
                            <tr>
                                <td> <?php echo $produkt['name']; ?> </td>
                                <td class="preis"> <?php echo $produkt['preis']; ?> </td>
                            </tr>
                <?php
                        }
                    }
                ?>
            </tbody>
        </table>
    </body>
</html>